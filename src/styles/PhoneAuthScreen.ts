import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
    },
    input: {
      marginBottom: 20,
    },
    button: {
      backgroundColor: '#2089dc',
      borderRadius: 5,
      marginVertical: 10,
      paddingHorizontal: 20,
      paddingVertical: 10,
    },
    verificationContainer: {
      marginTop: 20,
      alignItems: 'center',
    },
    codeInputContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginVertical: 20,
    },
    codeInput: {
      width: 40,
      marginHorizontal: 5,
    },
    codeInputText: {
      textAlign: 'center',
      fontSize: 18,
    },
    errorMessage: {
      color: 'red',
      marginTop: 10,
    },
  });