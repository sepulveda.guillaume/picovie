import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  task: {
    fontSize: 20,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#ff5c5c',
    width: 200,
  },
});
