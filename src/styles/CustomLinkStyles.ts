import { StyleSheet } from "react-native";

export default StyleSheet.create({
  pressable: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 3,
    padding: 8,
    backgroundColor: "#ff5c5c",
    borderColor: "#2089dc",
    borderWidth: 0,
    width: 200,
  },
  text: {
    color: "#fff",
    fontSize: 18,
    textAlign: "center",
    paddingTop: 1,
    paddingBottom: 1,
  },
});
