import firestore from '@react-native-firebase/firestore';

const addUserToDatabase = async (userData: any) => {
  try {
    await firestore().collection('users').add(userData);
  } catch (error) {
    throw new Error((error as Error).message);
  }
};

export default addUserToDatabase;