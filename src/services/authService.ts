import { auth } from "../firebase/firebaseConfig";
import {
  signInWithPhoneNumber,
  RecaptchaVerifier,
  PhoneAuthProvider,
  signInWithCredential,
  ConfirmationResult,
  onAuthStateChanged,
} from "firebase/auth";

console.log("auth", auth);

export const setupRecaptchaVerifier = (containerId: string) => {
  const verifier = new RecaptchaVerifier(auth, containerId, {
    size: "invisible",
    callback: (response: Response) => {
      // reCAPTCHA solved, allow signInWithPhoneNumber.
      console.log(`reCAPTCHA solved: ${response}`);
    },
  });
  verifier.render();
  return verifier;
};

export const sendVerificationCode = async (
  phoneNumber: string,
  recaptchaVerifier: RecaptchaVerifier
): Promise<ConfirmationResult> => {
  try {
    const confirmationResult = await signInWithPhoneNumber(
      auth,
      phoneNumber,
      recaptchaVerifier
    );
    return confirmationResult;
  } catch (error) {
    console.error("SMS not sent", error);
    throw error;
  }
};

export const confirmCode = async (verificationId: string, code: string) => {
  try {
    const credential = PhoneAuthProvider.credential(verificationId, code);
    await signInWithCredential(auth, credential);
  } catch (error) {
    console.error("Error confirming code", error);
    throw error;
  }
};

export const authStateListener = (
  onUserAuthenticated: (user: any) => void,
  onUserNotAuthenticated: () => void
) => {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      onUserAuthenticated(user);
    } else {
      onUserNotAuthenticated();
    }
  });
};
