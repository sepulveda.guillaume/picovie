import { createContext, useContext, useState, ReactNode } from "react";

// Définir le type pour les données du jeu
interface GameData {
  // Ajoutez ici les données spécifiques à votre jeu
}

// Définir le type pour le contexte du jeu
interface GameContextType {
  gameData: GameData;
  updateGameData: (newGameData: GameData) => void;
}

interface GameProviderProps {
  children: ReactNode;
}

// Créer le contexte du jeu
const GameContext = createContext<GameContextType | undefined>(undefined);

// Créer un fournisseur pour le contexte du jeu
export const GameProvider = ({ children }: GameProviderProps) => {
  const [gameData, setGameData] = useState<GameData>({
    /* Initialisation des données du jeu */
  });

  // Fonction pour mettre à jour les données du jeu
  const updateGameData = (newGameData: GameData) => {
    setGameData(newGameData);
  };

  // Retourne le fournisseur avec le contexte et les données du jeu
  return (
    <GameContext.Provider value={{ gameData, updateGameData }}>
      {children}
    </GameContext.Provider>
  );
};

// Hook personnalisé pour accéder au contexte du jeu
export const useGame = () => {
  const context = useContext(GameContext);
  if (context === undefined) {
    throw new Error("useGame must be used within a GameProvider");
  }
  return context;
};
