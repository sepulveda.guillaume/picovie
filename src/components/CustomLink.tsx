import { Pressable, Text } from "react-native";
import { Link } from "expo-router";
import styles from "../styles/CustomLinkStyles";

interface CustomLinkProps {
  href: string;
  parameters?: Record<string, any>;
  asChild: boolean;
  label: string;
}

export default function CustomLink({
  href,
  parameters,
  asChild,
  label,
}: CustomLinkProps) {
  return (
    <Link
      href={{
        pathname: href,
        params: parameters,
      }}
      asChild={asChild}
    >
      <Pressable style={styles.pressable}>
        <Text style={styles.text}>{label}</Text>
      </Pressable>
    </Link>
  );
}
