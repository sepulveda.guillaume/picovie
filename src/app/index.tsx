import { useState } from "react";
import { View, TextInput } from "react-native";
import {
  sendVerificationCode,
  setupRecaptchaVerifier,
} from "../services/authService";
import styles from "../styles/PhoneAuthScreen";
import { RecaptchaVerifier } from "firebase/auth";
import { useRouter, useFocusEffect } from "expo-router";
import { Button, Input, Text } from "react-native-elements";

export default function PhoneAuthScreen() {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [verificationCode, setVerificationCode] = useState(Array(6).fill(""));
  const [message, setMessage] = useState<string>("");
  const [confirmationResult, setConfirmationResult] = useState<any>(null);
  const [redirectToHome, setRedirectToHome] = useState<boolean>(false);
  const [isRequestingCode, setIsRequestingCode] = useState(false);

  const router = useRouter();

  const recaptchaContainerId = "recaptcha-container";

  useFocusEffect(() => {
    if (redirectToHome) router.replace("/home");
  });

  const handleSendCode = async () => {
    const fullPhoneNumber = `+33${phoneNumber}`;
    setIsRequestingCode(true);
    try {
      const verifier = setupRecaptchaVerifier(
        recaptchaContainerId
      ) as unknown as RecaptchaVerifier;
      const result = await sendVerificationCode(fullPhoneNumber, verifier);
      setConfirmationResult(result);
      setMessage("Code de vérification envoyé sur votre téléphone.");
    } catch (error) {
      setMessage(`Erreur: ${(error as Error).message}`);
    } finally {
      setIsRequestingCode(false);
    }
  };

  const handleConfirmCode = async () => {
    if (confirmationResult) {
      try {
        await confirmationResult.confirm(verificationCode);
        setMessage("Code de vérification confirmé.");
        setRedirectToHome(true);
      } catch (error) {
        setMessage(`Erreur: ${(error as Error).message}`);
      }
    }
  };

  const handleInputChange = (index: number, value: string) => {
    if (/^\d*$/.test(value)) {
      const newCode = [...verificationCode];
      newCode[index] = value;
      setVerificationCode(newCode);
    }
  };

  return (
    <View style={styles.container}>
      <div id={recaptchaContainerId}></div>
      <Input
        label="Numéro de téléphone"
        value={phoneNumber}
        onChangeText={(text) => setPhoneNumber(text)}
        maxLength={9}
        keyboardType="phone-pad"
        containerStyle={styles.input}
        leftIcon={<Text>+33</Text>}
      />
      <Button
        title="Envoyer le code"
        onPress={handleSendCode}
        disabled={isRequestingCode}
      />
      {confirmationResult && (
        <View style={styles.verificationContainer}>
          <Text h4>Code de vérification</Text>
          <View style={styles.codeInputContainer}>
            {verificationCode.map((digit, index) => (
              <Input
                key={index}
                value={digit}
                onChangeText={(text) => handleInputChange(index, text)}
                maxLength={1}
                keyboardType="number-pad"
                containerStyle={styles.codeInput}
                inputStyle={styles.codeInputText}
              />
            ))}
          </View>
          <Button
            title="Vérifier le code"
            onPress={handleConfirmCode}
            buttonStyle={styles.button}
          />
        </View>
      )}
      <Text>{message}</Text>
    </View>
  );
}
