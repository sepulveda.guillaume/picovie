import React, { useState } from "react";
import { View, Text, Pressable } from "react-native";
import { Input, Slider } from "react-native-elements";
import styles from "../../styles/OptionsScreenStyles";
import { Link } from "expo-router";
import CustomLink from "@/src/components/CustomLink";

export default function OptionsScreen() {
  const [players, setPlayers] = useState(2);
  const [difficulty, setDifficulty] = useState("facile");

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Options de jeu</Text>
      <Input
        label="Nombre de joueurs"
        value={players.toString()}
        onChangeText={(text) => setPlayers(parseInt(text))}
        keyboardType="numeric"
        containerStyle={styles.input}
      />
      <Text style={styles.label}>Difficulté</Text>
      <Slider
        value={difficulty === "facile" ? 0 : difficulty === "moyen" ? 1 : 2}
        onValueChange={(value) =>
          setDifficulty(
            value === 0 ? "facile" : value === 1 ? "moyen" : "difficile"
          )
        }
        step={1}
        maximumValue={2}
        minimumValue={0}
        style={styles.slider}
      />
      <CustomLink
        href="/game"
        parameters={{ players, difficulty }}
        asChild
        label="Commencer le jeu"
      />
    </View>
  );
}
