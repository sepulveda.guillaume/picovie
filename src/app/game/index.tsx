import React, { useState } from "react";
import { View, Text } from "react-native";
import { Button } from "react-native-elements";
import { useLocalSearchParams } from 'expo-router';
import styles from "../../styles/GameScreenStyles";

interface GameScreenProps {
  players: string;
  difficulty: "facile" | "moyen" | "difficile";
}

export default function GameScreen() {
  const { players, difficulty } = useLocalSearchParams() as unknown as GameScreenProps;
  const [currentPlayer, setCurrentPlayer] = useState(1);
  const [currentTask, setCurrentTask] = useState("Faites un toast !");

  const nextTask = () => {
    setCurrentPlayer((currentPlayer) => currentPlayer + 1);
    setCurrentTask("Faites un toast !");
  };

  const numberOfPlayers: number = parseInt(players);

  if (isNaN(numberOfPlayers) || !["facile", "moyen", "difficile"].includes(difficulty)) {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Erreur dans les paramètres</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Joueurs en jeu: {players}, difficulté: {difficulty}</Text>
      <Text style={styles.title}>Joueur {currentPlayer}</Text>
      <Text style={styles.task}>{currentTask}</Text>
      <Button
        title="Tâche suivante"
        onPress={nextTask}
        buttonStyle={styles.button}
      />
    </View>
  );
}
