import React from "react";
import { View, Text } from "react-native";
import styles from "../../styles/HomeScreenStyles";
import CustomLink from "../../components/CustomLink";

export default function HomeScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Bienvenue sur Picovie !</Text>
      <CustomLink href="/options" asChild label="Options de jeu" />
    </View>
  );
}
