import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type RootStackParamList = {
  Home: undefined;
  Options: undefined;
  Game: { players: number; difficulty: string };
  '+not-found': undefined;
};

export type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Home'>;
export type OptionsScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Options'>;
export type GameScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Game'>;
export type NotFoundScreenNavigationProp = StackNavigationProp<RootStackParamList, '+not-found'>;